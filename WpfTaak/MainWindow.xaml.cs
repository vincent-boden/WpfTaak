﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WpfTaak
{
    public partial class MainWindow : Window
    {
        ObservableCollection<TodoItem> todoList = new ObservableCollection<TodoItem>() {
             new TodoItem() { Beschrijving = "ListBox testen", Done = false },
             new TodoItem() { Beschrijving = "ItemTemplate voor ListBox maken", Done = false},
             new TodoItem() { Beschrijving = "...", Done = false}
        };

        public MainWindow()
        {
            InitializeComponent();
            todoItemsListBox.ItemsSource = todoList;
        }

        private void removeButton_Click(object sender, RoutedEventArgs e)
        {
            if (todoItemsListBox.SelectedItem != null)
            {
                todoList.Remove(todoItemsListBox.SelectedItem as TodoItem);
            }
        }

        private void addButton_Click(object sender, RoutedEventArgs e)
        {
            todoList.Add(new TodoItem() { Beschrijving = textBox.Text, Done = false });
        }

        private void changeButton_Click(object sender, RoutedEventArgs e)
        {
            //Code niet gevonden
        }
    }
}
